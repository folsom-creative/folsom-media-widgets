<?php
/*
Widget Name: Video Modal
Description: Display a video in a modal with a custom thumbnail.
Author: Folsom Creative
Author URI: https://www.folsomcreative.com/
*/

class Folsom_Widgets_Video_Modal_Widget extends SiteOrigin_Widget {

	function __construct() {
		parent::__construct(
			'folsom-video-modal',
			__('Video Modal', 'folsom-widgets-bundle'),
			array(
				'description' => __('Embed a Youtube or Vimeo video in a modal with a custom thumbnail.', 'folsom-widgets-bundle'),
				'panels_groups' => array('folsom'),
			),
			array(

			),
			false,
			plugin_dir_path( __FILE__ )
		);
	}

	function initialize(){
		$this->register_frontend_styles( array(
			array(
				'folsom-video-modal-css',
				plugin_dir_url(__FILE__) . 'css/style.css'
			),
			array(
				'fancybox-css',
				plugin_dir_url(__FILE__) . 'css/jquery.fancybox.min.css'
			)
		) );
		$this->register_frontend_scripts(
			array(
				array(
					'fancybox-js',
					plugin_dir_url( __FILE__ ) . 'js/jquery.fancybox.min.js',
					array( 'jquery' )
				)
			)
		);
	}

	function get_widget_form(){
		return array(
			'title' => array(
				'type' => 'text',
				'label' => __('Title', 'folsom-widgets-bundle'),
			),
			'image' => array(
				'type' => 'media',
				'label' => __('Image', 'folsom-widgets-bundle'),
				'description' => __('The best image has a 16 by 9 aspect ratio. The best image is 800px wide by 450px tall. Uncropped images may slow down page speed.', 'folsom-widgets-bundle'),
			),
			'url' => array(
				'type' => 'text',
				'label' => __('Video URL', 'folsom-widgets-bundle'),
				'description' => __('Enter a Youtube or Vimeo URL', 'folsom-widgets-bundle'),
			),
			'button_theme' => array(
				'type' => 'select',
				'label' => __('Play Button Theme', 'folsom-widgets-bundle'),
				'description' => __('Choose a light theme for a white button and dark theme for a black button.', 'folsom-widgets-bundle'),
				'default' => 'light',
				'options' => array(
            'light' => __( 'Light Theme', 'folsom-widgets-bundle' ),
            'dark' => __( 'Dark Theme', 'folsom-widgets-bundle' ),
        )
			),
			'title_position' => array(
				'type' => 'radio',
				'label' => __( 'Title Position', 'folsom-widgets-bundle' ),
				'description' => __( 'Choose where to place the title in relation to the video thumbnail.', 'folsom-widgets-bundle' ),
				'default' => 'below',
				'options' => array(
					'below' => __( 'Below', 'folsom-widgets-bundle' ),
					'above' => __( 'Above', 'folsom-widgets-bundle' ),
				)
			),
		);
	}

	function get_template_variables( $instance, $args ){
		return array(
			'video' => !empty($instance['video']) ? $instance['video'] : array(),
		);
	}

	function video_thumbnail( $image_id ){
		if ( ! empty( $image_id ) ) {
			$src = wp_get_attachment_image_src( $image_id, 'full' );
			return esc_url( $src[0]);
		}
	}

}

siteorigin_widget_register( 'folsom-video-modal', __FILE__, 'Folsom_Widgets_Video_Modal_Widget' );
