<?php
/**
 * @var $design
 * @var $settings
 * @var $testimonials
 */
?>
<?php if( !empty( $instance['title'] ) && $instance['title_position'] == 'above' ) echo $args['before_title'] . esc_html($instance['title']) . $args['after_title'] ?>

<div class="video-modal-container">
		<?php
			$image_id = $instance['image'];
		?>
		<a data-fancybox class="video-modal <?php echo $instance['button_theme']; ?>" style="background-image: url(<?php echo $this->video_thumbnail( $image_id ); ?>);" href="<?php echo $instance['url']; ?>"></a>
</div>

<?php if( !empty( $instance['title'] ) && $instance['title_position'] == 'below' ) echo $args['before_title'] . esc_html($instance['title']) . $args['after_title'] ?>