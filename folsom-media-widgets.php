<?php
/*
Plugin Name: Folsom Widgets Bundle
Description: A collection of widgets for SiteOrigin Page Builder.
Version: 1.0.1
Author: Folsom Creative
Author URI: https://www.folsomcreative.com/
Plugin URI: https://www.folsomcreative.com/
License: GPL3
License URI: https://www.gnu.org/licenses/gpl-3.0.txt
*/

function so_widgets_experimental_add_folder($folders){
  $folders[] = plugin_dir_path(__FILE__).'widgets/';
  return $folders;
}
add_filter('siteorigin_widgets_widget_folders', 'so_widgets_experimental_add_folder');

function mytheme_add_widget_tabs($tabs) {
    $tabs[] = array(
        'title' => __('Folsom Widgets', 'folsom'),
        'filter' => array(
            'groups' => array('folsom')
        )
    );

    return $tabs;
}
add_filter('siteorigin_panels_widget_dialog_tabs', 'mytheme_add_widget_tabs', 20);
